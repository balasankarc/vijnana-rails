class ChangeYearInItems < ActiveRecord::Migration
  def change
    change_column :items, :year, :integer
  end
end
