class AddHodIdToDepartments < ActiveRecord::Migration
  def change
    add_column :departments, :hod_id, :integer
  end
end
