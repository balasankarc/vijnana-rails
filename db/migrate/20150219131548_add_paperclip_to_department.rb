class AddPaperclipToDepartment < ActiveRecord::Migration
  def change
      add_attachment :departments, :image
  end
end
