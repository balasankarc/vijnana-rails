class ChangeSubjectIdToDepartmentIdItems < ActiveRecord::Migration
  def change
      rename_column :items, :subject_id,:department_id 
  end
end
