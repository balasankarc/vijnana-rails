class CreateSubjectrelations < ActiveRecord::Migration
  def change
    create_table :subjectrelations do |t|
      t.integer :user_id
      t.integer :subject_id

      t.timestamps
    end
  end
end
