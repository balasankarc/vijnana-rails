class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :code
      t.string :name
      t.integer :credit
      t.text :description
      t.integer :faculty_id
      t.integer :department_id
      t.integer :course_id
      t.integer :semester_id

      t.timestamps
    end
  end
end
