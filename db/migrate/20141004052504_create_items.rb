class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.string :year
      t.integer :type_id
      t.integer :user_id
      t.integer :subject_id

      t.timestamps
    end
  end
end
