class AddBatchToUsers < ActiveRecord::Migration
  def change
    add_column :users, :batchbegin, :date
    add_column :users, :batchend, :date
  end
end
