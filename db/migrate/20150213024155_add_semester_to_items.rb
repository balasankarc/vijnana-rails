class AddSemesterToItems < ActiveRecord::Migration
  def change
    add_column :items, :semester_id, :int
  end
end
