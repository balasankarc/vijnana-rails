class AddDetailsToUsers < ActiveRecord::Migration
  def change
      add_column :users, :admissionnumber, :string
      add_column :users, :dob, :date
      add_column :users, :address, :text
      add_column :users, :bloodgroup, :string
      add_column :users, :email, :string
      add_column :users, :phone, :string
      add_column :users, :mobile, :string
  end
end
