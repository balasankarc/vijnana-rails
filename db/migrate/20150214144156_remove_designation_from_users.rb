class RemoveDesignationFromUsers < ActiveRecord::Migration
  def up
      remove_column :users, :designation
  end
  def down
      add_column :users, :designation, :string
  end
end
