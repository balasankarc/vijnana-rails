class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :name
      t.string :designation
      t.string :department
      t.boolean :admin

      t.timestamps
    end
  end
end
