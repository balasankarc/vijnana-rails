class AddPrivateprofileToUsers < ActiveRecord::Migration
  def change
    add_column :users, :privateprofile, :boolean, :default=>true
  end
end
