class DepartmentsController < ApplicationController
  before_action :set_department, only: [:show, :edit, :update, :destroy, :changedepimage]
  # GET /departments
  # GET /departments.json
  def index
    @departments = Department.all
  end

  def changedepimage
  end

  # GET /departments/1
  # GET /departments/1.json
  def show
        @items = Item.all
        @users = User.where("department_id = ? and designation = 'student'",@department.id)
        @facultyusers = User.where("department_id = ? and designation = 'faculty'",@department.id)
        @conditions={}
        if params.has_key?(:type) then
            puts "Type is there"
            @conditions["type"]=params[:type]
        end
        if params.has_key?(:course) then
            @conditions["course"]=params[:course]
        end
        if params.has_key?(:semester) then
            @conditions["semester"]=params[:semester]
        end
        puts @conditions
        @querystring="department_id = " + @department.id.to_s
        @conditions.each do |key,value|
            if not value=='' then
                if not @querystring=="" then
                    @querystring = @querystring + " and "
                end
                if key=="type" then
                    @typeid = Type.where("name = ?",value).first.id
                    @querystring = @querystring+"type_id = "+@typeid.to_s
                elsif key=="course" then
                    @courseid = Course.where("name = ?",value).first.id
                    @querystring = @querystring+"course_id = "+@courseid.to_s
                elsif key=="semester" then
                    @semid = Semester.where("name = ?",value).first.id
                    @querystring = @querystring+"semester_id = "+@semid.to_s
                end
            end
        end
        puts @querystring
        @items = Item.where(@querystring)
  end

  # GET /departments/new
  def new
    @department = Department.new
  end

  # GET /departments/1/edit
  def edit
  end

  # POST /departments
  # POST /departments.json
  def create
    @department = Department.new(department_params)
    respond_to do |format|
      if @department.save
        format.html { redirect_to @department, notice: 'Department was successfully created.' }
        format.json { render :show, status: :created, location: @department }
      else
        format.html { render :new }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /departments/1
  # PATCH/PUT /departments/1.json
  def update
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to @department, notice: 'Department was successfully updated.' }
        format.json { render :show, status: :ok, location: @department }
      else
        format.html { render :edit }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /departments/1
  # DELETE /departments/1.json
  def destroy
    @department.items.each do |item|
        item.destroy
    end
    @department.destroy
    respond_to do |format|
      format.html { redirect_to departments_url, notice: 'Department was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def department_params
      params.require(:department).permit(:name, :image, :hod_id)
    end
end
