class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :changepassword, :changepasswordfunction, :removeprofilepic, :changeimage, :mysubjects]
  helper_method :isuploader, :isadmin, :isuseruploader

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  def login
  end

  def signin
      @username = params[:user][:username]
      @password = params[:user][:password]
      @password_input_sha = Digest::SHA1.hexdigest(params[:user][:password]) 
      @users = User.where(:username => @username)
      if not @users.empty?
          @currentuser = @users.first
          if @currentuser.password == @password_input_sha then
              session[:user] = @users.first.username
              redirect_to root_path
          else
              redirect_to sign_in_path, notice:"Incorrect"
          end
      else
          redirect_to sign_in_path, notice:"Incorrect"
      end
  end

  def admin
  end

  def logout
      session.delete(:user)
      redirect_to root_path
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @password_input = params[:user][:password]
    @password_input_confirmation = params[:user][:password_confirmation]
    @password_input_sha = Digest::SHA1.hexdigest(@password_input)
    @password_input_confirmation_sha = Digest::SHA1.hexdigest(@password_input_confirmation)
    @newparams = user_params
    @newparams[:password] = @password_input_sha
    @newparams[:password_confirmation] = @password_input_confirmation_sha
    @user = User.new(@newparams)
    @user.department = Department.where("name = ?",params[:user][:department][:name]).first
    @user.course = Course.where("name = ?",params[:user][:course][:name]).first if params[:user][:course]
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
        @params = user_params
        @params[:batchbegin] = DateTime.new(params[:user][:batchbegin].to_i) if params[:user][:batchbegin]
        @params[:batchend] = DateTime.new(params[:user][:batchend].to_i) if params[:user][:batchend]
      if @user.update(@params)
        @user.department = Department.where("name = ?",params[:user][:department][:name]).first if params[:user][:department]
        @user.course = Course.where("name = ?",params[:user][:course][:name]).first if params[:user][:course]
        @user.save
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def removeprofilepic
      @user.image = nil
      @user.save
      redirect_to @user
  end

  def changeimage
  end

  def changepassword
  end

  def changepasswordfunction
      puts params[:user][:password]
      @currentpassword = params[:user][:password]
      @newpassword_input = params[:user][:newpassword]
      @newpassword_input_confirmation = params[:user][:newpassword_confirmation]
      @currentpassword_sha = Digest::SHA1.hexdigest(@currentpassword)
      if @currentpassword_sha != @user.password then
          redirect_to changepassword_path, notice:"Current Password Incorrect"
      else
          if @newpassword_input != @newpassword_input_confirmation then
              redirect_to changepassword_path, notice: "Passwords do not match"
          else
              @newpassword_input_sha = Digest::SHA1.hexdigest(@newpassword_input)
              @user.update_attribute(:password,@newpassword_input_sha)
              redirect_to @user
          end
      end

  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if @user == User.where("username = ?",session[:user])
        session.delete(:user)
    end
    @user.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      if params[:id]
          @user = User.find(params[:id])
      else
          @user = User.where("username = ?",session[:user]).first
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :password, :name, :designation, :department, :admin, :password_confirmation, :newpassword, :new_password_confirmation, :image, :admissionnumber, :dob, :address, :bloodgroup, :email, :phone, :mobile, :privateprofile, :course, :batchbegin, :batchend)
    end
end
