class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.all
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
        @items = Item.all
        @conditions={}
        if params.has_key?(:department) then
            @conditions["department"]=params[:department]
        end
        if params.has_key?(:type) then
            @conditions["type"]=params[:type]
        end
        if params.has_key?(:semester) then
            @conditions["semester"]=params[:semester]
        end
        puts @conditions
        @querystring="course_id = " + @course.id.to_s
        @conditions.each do |key,value|
            if not value=='' then
                if not @querystring=="" then
                    @querystring = @querystring + " and "
                end
                if key=="department" then
                    @departmentid = Department.where("name = ?",value).first.id
                    @querystring = @querystring+"department_id = "+@departmentid.to_s
                elsif key=="type" then
                    @typeid = Type.where("name = ?",value).first.id
                    @querystring = @querystring+"type_id = "+@typeid.to_s
                elsif key=="semester" then
                    @semid = Semester.where("name = ?",value).first.id
                    @querystring = @querystring+"semester_id = "+@semid.to_s
                end
            end
        end
        puts @querystring
        @items = Item.where(@querystring)


  end

  # GET /courses/new
  def new
    @course = Course.new
  end

  # GET /courses/1/edit
  def edit
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)

    respond_to do |format|
      if @course.save
        format.html { redirect_to @course, notice: 'Course was successfully created.' }
        format.json { render :show, status: :created, location: @course }
      else
        format.html { render :new }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    respond_to do |format|
      if @course.update(course_params)
        format.html { redirect_to @course, notice: 'Course was successfully updated.' }
        format.json { render :show, status: :ok, location: @course }
      else
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.destroy
    respond_to do |format|
      format.html { redirect_to courses_url, notice: 'Course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:name, :image)
    end
end
