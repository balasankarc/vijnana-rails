class HomeController < ApplicationController
  def index
  end

  def about
  end

  def statistics
  end

  def coursestatistics
  end

  def departmentstatistics
  end

  def typestatistics
  end
end
