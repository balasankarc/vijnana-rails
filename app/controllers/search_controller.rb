class SearchController < ApplicationController

    def index
        @type = "Items"
        @items = Item.all
        @conditions={}
        if params.has_key?(:type) then
            @conditions["type"]=params[:type]
        end
        if params.has_key?(:course) then
            @conditions["course"]=params[:course]
        end
        if params.has_key?(:department) then
            @conditions["department"]=params[:department]
        end
        if params.has_key?(:semester) then
            @conditions["semester"]=params[:semester]
        end
        if params[:query] then
            @searchquery = params[:query]
        else
            @searchquery = ""
        end
        @corrected = @searchquery.scan(/.{1}|.+/).join("%")
        @querystring = "title LIKE '" + "%#{@corrected}%" + "'"
        @conditions.each do |key,value|
            if not value=='' then
                if not @querystring=="" then
                    @querystring = @querystring + " and "
                end
                if key=="type" then
                    @typeid = Type.where("name = ?",value).first.id
                    @querystring = @querystring+"type_id = "+@typeid.to_s
                elsif key=="course" then
                    @courseid = Course.where("name = ?",value).first.id
                    @querystring = @querystring+"course_id = "+@courseid.to_s
                elsif key=="department" then
                    @depid = Department.where("name = ?",value).first.id
                    @querystring = @querystring+"department_id = "+@depid.to_s
                elsif key=="semester" then
                    @semid = Semester.where("name = ?",value).first.id
                    @querystring = @querystring+"semester_id = "+@semid.to_s
                end
            end
        end
        puts @querystring
        @type = params[:type]
        @items = Item.where(@querystring)

    end
end
