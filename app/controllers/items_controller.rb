class ItemsController < ApplicationController
    before_action :set_item, only: [:show, :edit, :update, :destroy, :adddetails]
    # GET /items
    # GET /items.json



    def index
        @type = "Items"
        @items = Item.all
        @conditions={}
        if params.has_key?(:type) then
            @conditions["type"]=params[:type]
        end
        if params.has_key?(:course) then
            @conditions["course"]=params[:course]
        end
        if params.has_key?(:department) then
            @conditions["department"]=params[:department]
        end
        if params.has_key?(:semester) then
            @conditions["semester"]=params[:semester]
        end
        puts @conditions
        @querystring=""
        @conditions.each do |key,value|
            if not value=='' then
                if not @querystring=="" then
                    @querystring = @querystring + " and "
                end
                if key=="type" then
                    @typeid = Type.where("name = ?",value).first.id
                    @querystring = @querystring+"type_id = "+@typeid.to_s
                elsif key=="course" then
                    @courseid = Course.where("name = ?",value).first.id
                    @querystring = @querystring+"course_id = "+@courseid.to_s
                elsif key=="department" then
                    @depid = Department.where("name = ?",value).first.id
                    @querystring = @querystring+"department_id = "+@depid.to_s
                elsif key=="semester" then
                    @semid = Semester.where("name = ?",value).first.id
                    @querystring = @querystring+"semester_id = "+@semid.to_s
                end
            end
        end
        puts @querystring
        @type = params[:type]
        @items = Item.where(@querystring)
    end

    # GET /items/1
    # GET /items/1.json
    def show
    end

    # GET /items/new
    def new
        @item = Item.new
    end

    # GET /items/1/edit
    def edit
    end

    # POST /items
    # POST /items.json
    def create
        @user = session[:user]
        @new_params = item_params
        @new_params[:user_id] = User.where("username = ?",@user).first.id
        @item = Item.new(@new_params)
        @types = Type.where("name = ?",params[:item][:type][:name])
        @item.type= @types.first
        @departments = Department.where("name = ?", params[:item][:department][:name])
        @semester = Semester.where("name = ?", params[:item][:semester][:name])
        @course = Course.where("name = ?", params[:item][:course][:name])
        @item.department = @departments.first
        @item.semester = @semester.first
        @item.course = @course.first
        @authors = params[:item][:author][:name].squish.gsub(", ",",").to_s.split(",")
        respond_to do |format|
            if @item.save
                @authors.each do |author|
                    @author_exist=Author.where("name = ?",author.to_s)
                    if @author_exist.empty?
                        @author_created=@item.authors.create(:name=>author.to_s)
                        @author_created.save
                    else
                        @item.authors<<@author_exist
                    end
                end
                if not params[:item][:subject]
                    format.html { redirect_to adddetails_path(@item), notice: 'Item was successfully updated.' }
                    format.json { render :show, status: :ok, location: @item }
                else
                    @subject = Subject.find(params[:item][:subject][:id]) if params[:item][:subject][:id]
                    @description = params[:item][:description] if params[:item][:description]
                    @item.subject = @subject if @subject
                    @item.description = @description if @description
                    @item.save
                    format.html { redirect_to @item, notice: 'Item was successfully updated.' }
                    format.json { render :show, status: :ok, location: @item }
                end
            else
                format.html { render :new }
                format.json { render json: @item.errors, status: :unprocessable_entity }
            end
        end
    end

    def adddetails
        @conditions = {}
        if @item.course then
            @conditions["course"]=@item.course.name
        end
        if @item.department then
            @conditions["department"]=@item.department.name
        end
        if @item.semester then
            @conditions["semester"]=@item.semester.name
        end
        puts @conditions
        @querystring=""
        @conditions.each do |key,value|
            if not value=='' then
                if not @querystring=="" then
                    @querystring = @querystring + " and "
                end
                if key=="course" then
                    @courseid = Course.where("name = ?",value).first.id
                    @querystring = @querystring+"course_id = "+@courseid.to_s
                elsif key=="department" then
                    @depid = Department.where("name = ?",value).first.id
                    @querystring = @querystring+"department_id = "+@depid.to_s
                elsif key=="semester" then
                    @semid = Semester.where("name = ?",value).first.id
                    @querystring = @querystring+"semester_id = "+@semid.to_s
                end
            end
        end
        puts @querystring
        @subjects = Subject.where(@querystring)
    end

    # PATCH/PUT /items/1
    # PATCH/PUT /items/1.json
    def update
        @authors = params[:item][:author][:name].squish.gsub(", ",",").to_s.split(",") if params[:item][:author]
        @parameters = item_params
        respond_to do |format|
            if @item.update(@parameters)
                @department = Department.where("name = ?", params[:item][:department][:name]).first if params[:item][:department]
                @semester = Semester.where("name = ?", params[:item][:semester][:name]).first if params[:item][:semester]
                @course = Course.where("name = ?", params[:item][:course][:name]).first if params[:item][:course]
                @item.course = @course if @course
                @item.department = @department if @department
                @item.semester = @semester if @semester
                @item.save
                if params[:item][:author]
                    @item.authors.each do |author|
                        @item.authors.delete(author)
                    end
                    @authors.each do |author|
                        @author_exist=Author.where("name = ?",author.to_s)
                        if @author_exist.empty?
                            @author_created=@item.authors.create(:name=>author.to_s)
                            @author_created.save
                        else
                            @item.authors<<@author_exist
                        end
                    end
                end
                if not params[:item][:subject]
                    format.html { redirect_to adddetails_path(@item), notice: 'Item was successfully updated.' }
                    format.json { render :show, status: :ok, location: @item }
                else
                    if params[:item][:subject][:id] and params[:item][:subject][:id] == "" then
                        puts "HERE"
                        @item.subject = nil
                        @item.save
                        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
                        format.json { render :show, status: :ok, location: @item }
                    else
                        @subject = Subject.find(params[:item][:subject][:id]) if params[:item][:subject][:id]
                        @description = params[:item][:description] if params[:item][:description]
                        @item.subject = @subject if @subject
                        @item.description = @description if @description
                        @item.save
                        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
                        format.json { render :show, status: :ok, location: @item }
                    end
                end
            else
                format.html { render :edit }
                format.json { render json: @item.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /items/1
    # DELETE /items/1.json
    def destroy
        @item.destroy
        respond_to do |format|
            format.html { redirect_to root_path, notice: 'Item was successfully destroyed.' }
            format.json { head :no_content }
        end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
        @item = Item.find_by_id!(params[:id])
    rescue
        redirect_to errors_file_not_found_path
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
        params.require(:item).permit(:title, :year, :type, :user, :department, :file, :author, :semester, :course, :subject, :description)
    end
end
