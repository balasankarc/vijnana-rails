class TypesController < ApplicationController
  before_action :set_type, only: [:show, :edit, :update, :destroy]

  # GET /types
  # GET /types.json
  def index
    @types = Type.all
  end

  # GET /types/1
  # GET /types/1.json
  def show
        @items = Item.all
        @conditions={}
        if params.has_key?(:department) then
            @conditions["department"]=params[:department]
        end
        if params.has_key?(:course) then
            @conditions["course"]=params[:course]
        end
        if params.has_key?(:semester) then
            @conditions["semester"]=params[:semester]
        end
        puts @conditions
        @querystring="type_id = " + @type.id.to_s
        @conditions.each do |key,value|
            if not value=='' then
                if not @querystring=="" then
                    @querystring = @querystring + " and "
                end
                if key=="department" then
                    @departmentid = Department.where("name = ?",value).first.id
                    @querystring = @querystring+"department_id = "+@departmentid.to_s
                elsif key=="course" then
                    @courseid = Course.where("name = ?",value).first.id
                    @querystring = @querystring+"course_id = "+@courseid.to_s
                elsif key=="semester" then
                    @semid = Semester.where("name = ?",value).first.id
                    @querystring = @querystring+"semester_id = "+@semid.to_s
                end
            end
        end
        puts @querystring
        @items = Item.where(@querystring)

  end

  # GET /types/new
  def new
    @type = Type.new
  end

  # GET /types/1/edit
  def edit
  end

  # POST /types
  # POST /types.json
  def create
    @type = Type.new(type_params)
    respond_to do |format|
      if @type.save
        format.html { redirect_to @type, notice: 'Type was successfully created.' }
        format.json { render :show, status: :created, location: @type }
      else
        format.html { render :new }
        format.json { render json: @type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /types/1
  # PATCH/PUT /types/1.json
  def update
    respond_to do |format|
      if @type.update(type_params)
        format.html { redirect_to @type, notice: 'Type was successfully updated.' }
        format.json { render :show, status: :ok, location: @type }
      else
        format.html { render :edit }
        format.json { render json: @type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /types/1
  # DELETE /types/1.json
  def destroy
    @type.destroy
    respond_to do |format|
      format.html { redirect_to types_url, notice: 'Type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type
      if not (params[:id] =~ /[a-zA-Z].*/) then
          @type = Type.find_by_id!(params[:id])
      else
          @name = params[:id].gsub("_"," ")
          puts @name
          @type = Type.where("name = ?",@name).first
          if @type.nil? then
              raise 
          end
      end
      rescue
        redirect_to errors_file_not_found_path
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def type_params
      params.require(:type).permit(:name,:image)
    end
end
