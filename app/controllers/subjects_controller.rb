class SubjectsController < ApplicationController
  before_action :set_subject, only: [:show, :edit, :update, :destroy, :choosestaff, :addstudenttosubject, :removeuser]

  # GET /subjects
  # GET /subjects.json
  def index
    @subjects = Subject.all
  end

  def removeuser
      if isadmin
      @subject.users.delete(params[:userid]) if params[:userid]
      end
      redirect_to @subject
  end

  helper_method :removeuser

  # GET /subjects/1
  # GET /subjects/1.json
  def show
  end

  # GET /subjects/new
  def new
    @subject = Subject.new
  end

  # GET /subjects/1/edit
  def edit
  end

  def choosestaff
    @staff = User.where("designation = 'faculty' and department_id = ?", @subject.department.id)
  end

  def addstudenttosubject
      @students = User.where("designation = 'student' and department_id = ?",@subject.department_id)
      puts @students
      @studentsfinal = []
      @students.each do |st|
          if not st.subjects.include?(@subject)
              @studentsfinal << st
          end
      end
      @students = @studentsfinal
      @parameter = params[:studentlist]
      puts @parameter
      if @parameter
      @parameter.each do |student|
          @stu = User.find(student)
          if not @stu.subjects.include?(@subject)
              @stu.subjects << @subject
          end
          @stu.save
      end
          redirect_to @subject
      end
#      redirect_to @subject
  end
  # POST /subjects
  # POST /subjects.json
  def create
    @subject = Subject.new(subject_params)
    @subject.department = Department.where("name = ?",params[:subject][:department][:name]).first if params[:subject][:department]
    @subject.course = Course.where("name = ?",params[:subject][:course][:name]).first if params[:subject][:course]
    @subject.semester = Semester.where("name = ?",params[:subject][:semester][:name]).first if params[:subject][:semester]
    respond_to do |format|
      if @subject.save
        format.html { redirect_to choosestaff_path(@subject), notice: 'Subject was successfully created.' }
        format.json { render :show, status: :created, location: @subject }
      else
        format.html { render :new }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subjects/1
  # PATCH/PUT /subjects/1.json
  def update
    respond_to do |format|
    @subject.department = Department.where("name = ?",params[:subject][:department][:name]).first if params[:subject][:department]
    @subject.course = Course.where("name = ?",params[:subject][:course][:name]).first if params[:subject][:course]
    @subject.semester = Semester.where("name = ?",params[:subject][:semester][:name]).first if params[:subject][:semester]
    @subject.save
      if @subject.update(subject_params)
        format.html { redirect_to @subject, notice: 'Subject was successfully updated.' }
        format.json { render :show, status: :ok, location: @subject }
      else
        format.html { render :edit }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subjects/1
  # DELETE /subjects/1.json
  def destroy
    @subject.destroy
    respond_to do |format|
      format.html { redirect_to subjects_url, notice: 'Subject was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject
      @subject = Subject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subject_params
      params.require(:subject).permit(:code, :name, :credit, :description, :faculty_id, :department, :course, :semester, :studentlist)
    end
end
