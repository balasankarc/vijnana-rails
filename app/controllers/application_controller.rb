class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :isadmin, :isuploader
  def isadmin
    if session[:user] and User.where("username = ?",session[:user]).first.designation=='admin' then
        return true
    else
        return false
    end
  end
  def isuploader
    if session[:user] and ["admin","faculty", "hod"].include?(User.where("username = ?",session[:user]).first.designation) then
        return true
    else
        return false
    end
  end

  def isuseruploader(user)
      if ["admin","faculty","hod"].include?(user.designation) then
          return true
      else
          return false
      end
  end
end
