module ApplicationHelper
  def isadmin
    if session[:user] and User.where("username = ?",session[:user]).first.designation=='admin' then
        return true
    else
        return false
    end
  end
  def isuploader
    if session[:user] and ["admin","faculty"].include?(User.where("username = ?",session[:user]).first.designation) then
        return true
    else
        return false
    end
  end

  def isfacultyofsubject(id)
      if session[:user] and Subject.find(id).faculty_id == User.where("username = ?",session[:user]).first.id then
          return true
      else
          return false
      end
  end

  def ishodofdepartment(id)
      if Subject.find(id).department then
          @dep = Subject.find(id).department
          if session[:user] and @dep.hod_id == User.where("username = ?", session[:user]).first.id then
              return true
          else
              return false
          end
      else
          return false
      end
  end
end
