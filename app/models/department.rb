class Department < ActiveRecord::Base
    has_many :items
    has_many :users
    has_many :subjects

    def isafaculty
        users.designation
    end


    has_attached_file :image, {:url => "/assets/images/departmentpics/:depname.:extension", :path=>":rails_root/public/assets/images/departmentpics/:depname.:extension"}
    validates_attachment_content_type :image, :content_type =>/image/
 
    Paperclip.interpolates('depname') do |attachment, style|
        attachment.instance.name.gsub(" ","_").gsub(".","")
    end
end
