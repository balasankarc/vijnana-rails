class Authorship < ActiveRecord::Base
    belongs_to :item
    belongs_to :author
end
