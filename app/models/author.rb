class Author < ActiveRecord::Base
    has_many :authorships
    has_many :items, through: :authorships
    accepts_nested_attributes_for :items
end
