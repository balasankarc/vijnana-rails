class Item < ActiveRecord::Base
    require 'bcrypt'
    belongs_to :type
    belongs_to :user
    belongs_to :department
    belongs_to :semester
    belongs_to :course
    belongs_to :subject
    
    has_many :authorships
    has_many :authors, through: :authorships
    accepts_nested_attributes_for :authors
    validates :department, presence: true
    validates :type, presence: true
    validates :file, presence: true, on: :create
    validates_inclusion_of :year, :in => 1900..3000, :message => "should be between 1900 and 3000"

    has_attached_file :file, {:url => "/items/:typename/:realname.:extension", :path=>":rails_root/public/items/:typename/:realname.:extension"}

    validates_attachment_content_type :file, content_type:["application/pdf","application/msword"]
    Paperclip.interpolates('realname') do |attachment, style|
        attachment.instance.title.gsub(" ","_").gsub(".","")+"_"+ Digest::SHA1.hexdigest(attachment.instance.user.username.to_s)
    end
    Paperclip.interpolates('typename') do |attachment, style|
        attachment.instance.type.name.gsub(" ","_").gsub(".","")
    end
end
