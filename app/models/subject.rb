class Subject < ActiveRecord::Base
    belongs_to :department
    belongs_to :course
    belongs_to :semester

    has_many :subjectrelations, class_name:'Subjectrelations'
    has_many :users, through: :subjectrelations
    accepts_nested_attributes_for :users

    has_many :items

    validates :department, presence:true
    validates :course, presence:true
    validates :semester, presence:true
end
