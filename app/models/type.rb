class Type < ActiveRecord::Base
    has_many :items
    has_attached_file :image, {:url => "/assets/images/typepics/:typname.:extension", :path=>":rails_root/public/assets/images/typepics/:typname.:extension"}
    validates_attachment_content_type :image, :content_type =>/image/
 
    Paperclip.interpolates('typname') do |attachment, style|
        attachment.instance.name.gsub(" ","_").gsub(".","")
    end

end
