class Semester < ActiveRecord::Base
    has_many :items
    has_many :subjects
end
