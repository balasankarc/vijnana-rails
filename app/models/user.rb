class User < ActiveRecord::Base
    has_many :items
    belongs_to :department
    belongs_to :course
    
    has_many :subjectrelations, class_name:'Subjectrelations'
    has_many :subjects, through: :subjectrelations
    accepts_nested_attributes_for :subjects

    validates :username, uniqueness: true
    validates :department_id, presence: true, if: :notadmin?
    validates :designation, presence: true
    validates :password, :confirmation => true
    validates :newpassword, :confirmation => true, :on => :changepassword
    validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :message => ": Format invalid", :on => :update
    validates_format_of :mobile, :with => /\A\w{10}\z/i, :message => ": Format invalid", :on => :update
    validates_format_of :phone, :with => /\A\w{0,4}[-]?\w{6,8}\z/i, :message => ": Format invalid", :on => :update
    scope :designation, ->{ where(designation:"faculty")}

    has_attached_file :image, :url => "/assets/images/profilepics/:usrname_:style.:extension", :path=>":rails_root/public/assets/images/profilepics/:usrname_:style.:extension", :styles => {:thumbnail => "25x25#", :small =>"125x125#", :large => "500x500>"}
    validates_attachment_content_type :image, :content_type =>/image/
 
    Paperclip.interpolates('usrname') do |attachment, style|
        attachment.instance.username.gsub(" ","_").gsub(".","")
    end

    def notadmin?
        if not designation == 'admin'
            return true
        else
            return false
        end
    end
end
