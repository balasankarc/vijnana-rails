class Course < ActiveRecord::Base
    has_many :items
    has_many :subjects
    has_attached_file :image, {:url => "/assets/images/coursepics/:coursname.:extension", :path=>":rails_root/public/assets/images/coursepics/:coursname.:extension"}
    validates_attachment_content_type :image, :content_type =>/image/
 
    Paperclip.interpolates('coursname') do |attachment, style|
        attachment.instance.name.gsub(" ","_").gsub(".","")
    end

end
