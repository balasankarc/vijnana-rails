json.array!(@items) do |item|
  json.extract! item, :id, :title, :year, :type_id, :user_id, :subject_id
  json.url item_url(item, format: :json)
end
