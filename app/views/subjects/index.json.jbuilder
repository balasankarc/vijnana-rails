json.array!(@subjects) do |subject|
  json.extract! subject, :id, :code, :name, :credit, :description, :faculty_id, :department_id, :course_id, :semester_id
  json.url subject_url(subject, format: :json)
end
