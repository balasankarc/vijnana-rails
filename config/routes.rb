Rails.application.routes.draw do

  get '/subjects', to: redirect('/departments')
  get '/semesters', to: redirect('/departments')
  get '/semesters/:id/', to: redirect('/departments')
  
  resources :subjects

  get 'errors/file_not_found'

  get 'errors/unprocessable'

  get 'errors/internal_server_error'

  resources :courses

  resources :semesters

  get 'search/index'

  post 'search/index'

  resources :authors

  resources :departments

  resources :types

  resources :users

  get 'home/index'

  resources :items

  root 'home#index'

  match '/files/original/missing.png', to: 'errors#file_not_found', via: :all, as: :missing

  get 'sign_in' => "users#login"

  post 'sign_in' => "users#signin"

  get 'logout' => "users#logout"

  get 'about' => "home#about"

  post 'items/index'

  patch 'users/:id/changepassword', to:'users#changepasswordfunction', as:'changepassword'

  get 'users/:id/changepassword', to:'users#changepassword'

  post 'departments/:id' => "departments#show"

  post 'types/:id' => "types#show"

  post 'courses/:id' => "courses#show"

  get 'admin' => 'users#admin', as: 'admin'

  get 'statistics' => 'home#statistics', as: 'statistics'

  get 'course_statistics' => 'home#coursestatistics'
  
  get 'department_statistics' => 'home#departmentstatistics'

  get 'type_statistics' => 'home#typestatistics'

  post 'users/:id/removeprofilepic' => 'users#removeprofilepic', as:'removeprofilepic'

  get 'users/:id/changeimage' => 'users#changeimage', as: 'changeimage'

  get 'subjects/:id/choosestaff' => 'subjects#choosestaff', as: 'choosestaff'
  
  get 'subjects/:id/addstudenttosubject' => 'subjects#addstudenttosubject', as: 'addstudenttosubject'
 
  patch 'subjects/:id/addstudenttosubject' => 'subjects#addstudenttosubject'

  get 'items/:id/adddetails' => 'items#adddetails', as: 'adddetails'

  get 'users/:id/mysubjects' => 'users#mysubjects', as: 'mysubjects'

  get 'departments/:id/changedepimage' => 'departments#changedepimage', as: 'changedepimage'

  get 'subjects/:id/removeuserpath' => 'subjects#removeuser', as: 'removeuser'

  match '/404', to: 'errors#file_not_found', via: :all
  
  match '/500', to: 'errors#internal_server_error', via: :all

  match '/422', to: 'errors#unprocessable', via: :all
end
