require 'test_helper'

class AuthorshipControllerTest < ActionController::TestCase
  test "should get item_id:integer" do
    get :item_id:integer
    assert_response :success
  end

  test "should get author_id:integer" do
    get :author_id:integer
    assert_response :success
  end

end
